## aosp_X01BD-eng 12 SP1A.210812.016 eng.androi.20211009.182311 test-keys
- Manufacturer: asus
- Platform: sdm660
- Codename: X01BD
- Brand: asus
- Flavor: aosp_X01BD-eng
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: eng.androi.20211009.182311
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/redfin/redfin:11/RQ3A.21101.001/7641976:user/release-keys
- OTA version: 
- Branch: aosp_X01BD-eng-12-SP1A.210812.016-eng.androi.20211009.182311-test-keys
- Repo: asus_x01bd_dump_2812


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
